/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactorysprites;

import java.util.ArrayList;

/**
 *
 * @author AndresWilT-PC
 */
public class AtaqueElfo implements Ataque{

    @Override
    public ArrayList<String> operacion() {
        ArrayList<String> AtqElf = new ArrayList<String>();
        AtqElf.add("/ImagenesElfo/AtqElf1.jpg");
        AtqElf.add("/ImagenesElfo/AtqElf2.jpg");
        AtqElf.add("/ImagenesElfo/AtqElf3.jpg");
        AtqElf.add("/ImagenesElfo/AtqElf4.jpg");
        AtqElf.add("/ImagenesElfo/AtqElf5.jpg");
        AtqElf.add("/Sonidos/ElfoAtaque.wav");
        return AtqElf;
    }
    
}
