/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactorysprites;

import java.util.ArrayList;

/**
 *
 * @author Nicolas Andrade
 */
public class AtaqueOrco implements Ataque {

    @Override
    public ArrayList<String> operacion() {
        ArrayList<String> AtqOrc = new ArrayList<String>();
        AtqOrc.add("/ImagenesOrco/AtqOrc1.jpg");
        AtqOrc.add("/ImagenesOrco/AtqOrc2.jpg");
        AtqOrc.add("/ImagenesOrco/AtqOrc3.jpg");
        AtqOrc.add("/Sonidos/OrcoAtaque.wav");
        return AtqOrc;
    }
}
