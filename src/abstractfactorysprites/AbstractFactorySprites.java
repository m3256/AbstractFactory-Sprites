/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactorysprites;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Nicolas Andrade
 */
public class AbstractFactorySprites {

    FabricaAbstracta fabrica;
    Salto salto;
    Izquierda izquierda;
    Derecha derecha;
    Ataque ataque;
    Muerte muerte;
    Scanner escaner;
    String Opcion;
    ArrayList<String> Derecha = new ArrayList<String>();
    ArrayList<String> Izquierda = new ArrayList<String>();
    ArrayList<String> Salto = new ArrayList<String>();
    ArrayList<String> Ataque = new ArrayList<String>();
    ArrayList<String> Muerte = new ArrayList<String>();
    
    public ArrayList<String> operacion(String opcion,String Operacion) {

        Opcion = opcion;
        switch (Opcion) {
            case "Humano":
                fabrica = new FabricaHumanos();
                break;
            case "Elfo":
                fabrica = new FabricaElfos();
                break;
            case "Orco":
                fabrica = new FabricaOrcos();
                break;
        }
        
        salto = fabrica.crearSalto();
        izquierda = fabrica.crearIzquierda();
        derecha = fabrica.crearDerecha();
        ataque = fabrica.crearAtaque();
        muerte = fabrica.crearMuerte();
        
        Salto=salto.operacion();
        Izquierda=izquierda.operacion();
        Derecha=derecha.operacion();
        Ataque=ataque.operacion();
        Muerte=muerte.operacion();
        
       if(Operacion.equals("izquierda"))
           return Izquierda;
       else if(Operacion.equals("derecha"))
           return Derecha;
       else if (Operacion.equals("ataque"))
           return Ataque;
       else if (Operacion.equals("salto"))
           return Salto;
       else
           return Muerte;
    }
}
