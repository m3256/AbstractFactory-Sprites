package abstractfactorysprites;

import java.util.ArrayList;

public class DerechaOrco implements Derecha {

    @Override
    public ArrayList<String> operacion() {
        ArrayList<String> DerOrc = new ArrayList<String>();
        DerOrc.add("/ImagenesOrco/DerOrc1.jpg");
        DerOrc.add("/ImagenesOrco/DerOrc2.jpg");
        DerOrc.add("/ImagenesOrco/DerOrc3.jpg");
        DerOrc.add("/ImagenesOrco/DerOrc4.jpg");
        DerOrc.add("/ImagenesOrco/DerOrc5.jpg");
        DerOrc.add("/Sonidos/Movimiento.wav");
        return DerOrc;
    }
}
