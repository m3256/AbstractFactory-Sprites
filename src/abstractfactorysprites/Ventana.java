/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactorysprites;

import java.applet.AudioClip;
import java.awt.Image;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Timer;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author Nicolas Andrade
 */
public class Ventana extends javax.swing.JFrame implements KeyListener, ActionListener {

    AbstractFactorySprites snd = new AbstractFactorySprites();

    static ArrayList<String> Componentes = new ArrayList<String>();
    public static JLabel Imagen = new JLabel();

    static Image img;

    public void CorrerHilo() {
        Vhilo mh = new Vhilo();
        Thread nuevoh = new Thread(mh);
        nuevoh.start();
        try {
            Thread.sleep(100);
        } catch (InterruptedException exc) {
            System.out.println("Hilo principal interrumpido.");
        }
    }

    public void build(String instruccion, String opcion) {
        for (int i = 0; i < snd.operacion(opcion, instruccion).size(); i++) {
            Componentes.add(snd.operacion(opcion, instruccion).get(i));
        }
    }

    public Ventana() {

        initComponents();
        this.setBounds(0, 0, 500, 500);

        //imagenes.add(snd.operacion(opcion));
        Imagen.setBounds(50, 50, 200, 200);
        getContentPane().add(Imagen);
        addKeyListener(this);
        setFocusable(false);

    }

    public void actionPerformed(ActionEvent e) {

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Elfo", "Humano", "Orco" }));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });
        jComboBox1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox1KeyPressed(evt);
            }
        });
        getContentPane().add(jComboBox1);
        jComboBox1.setBounds(10, 20, 91, 22);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed

    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jComboBox1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox1KeyPressed
        keyPressed(evt);
    }//GEN-LAST:event_jComboBox1KeyPressed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox jComboBox1;
    // End of variables declaration//GEN-END:variables

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        AudioClip sonido;
        int n;
        String instruccion;
        String opcion;
        String sound;
        opcion = (String) jComboBox1.getSelectedItem();
        Componentes.clear();
        int c = e.getKeyCode();
        if (c == KeyEvent.VK_UP) {
            instruccion = "salto";
            this.build(instruccion, opcion);
            this.CorrerHilo();
            n = Componentes.size()-1;
            sound = Componentes.get(n);
            System.out.println(sound);
            sonido = java.applet.Applet.newAudioClip(getClass().getResource(Componentes.get(n)));
            sonido.play();
        } else if (c == KeyEvent.VK_LEFT) {
            instruccion = "izquierda";
            this.build(instruccion, opcion);
            this.CorrerHilo();
            n = Componentes.size()-1;
            sound = Componentes.get(n);
            System.out.println(sound);
            sonido = java.applet.Applet.newAudioClip(getClass().getResource(Componentes.get(n)));
            sonido.play();
        } else if (c == KeyEvent.VK_RIGHT) {
            instruccion = "derecha";
            this.build(instruccion, opcion);
            this.CorrerHilo();
            n = Componentes.size()-1;
            sound = Componentes.get(n);
            System.out.println(sound);
            sonido = java.applet.Applet.newAudioClip(getClass().getResource(Componentes.get(n)));
            sonido.play();
        } else if (c == KeyEvent.VK_X) {
            instruccion = "ataque";
            this.build(instruccion, opcion);
            this.CorrerHilo();
           n = Componentes.size()-1;
            sound = Componentes.get(n);
            System.out.println(sound);
            sonido = java.applet.Applet.newAudioClip(getClass().getResource(Componentes.get(n)));
            sonido.play();
        } else if (c == KeyEvent.VK_DELETE) {
            instruccion = "muerte";
            this.build(instruccion, opcion);
            this.CorrerHilo();
           n = Componentes.size()-1;
            sound = Componentes.get(n);
            System.out.println(sound);
            sonido = java.applet.Applet.newAudioClip(getClass().getResource(Componentes.get(n)));
            sonido.play();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

}
