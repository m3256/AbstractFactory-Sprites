/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactorysprites;

import java.util.ArrayList;


public class MuerteOrco implements Muerte {

    @Override
    public ArrayList<String> operacion() {
        ArrayList<String> MuerOrc = new ArrayList<String>();
        MuerOrc.add("/ImagenesOrco/MuerOrc1.jpg");
        MuerOrc.add("/ImagenesOrco/MuerOrc2.jpg");
        MuerOrc.add("/ImagenesOrco/MuerOrc3.jpg");
        MuerOrc.add("/ImagenesOrco/MuerOrc4.jpg");  
        MuerOrc.add("/Sonidos/OrcoMuerte.wav");
        return MuerOrc;
    }
    
}
