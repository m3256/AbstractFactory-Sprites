/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactorysprites;

import java.util.ArrayList;

/**
 *
 * @author famil
 */
public class MuerteElfo implements Muerte {

     @Override
     public ArrayList<String> operacion() {
        ArrayList<String> MuerElf = new ArrayList<String>();
        MuerElf.add("/ImagenesElfo/ElfMuer1.jpg");
        MuerElf.add("/ImagenesElfo/ElfMuer2.jpg");
        MuerElf.add("/ImagenesElfo/ElfMuer3.jpg");
        MuerElf.add("/ImagenesElfo/ElfMuer4.jpg");
        MuerElf.add("/ImagenesElfo/ElfMuer5.jpg");
        MuerElf.add("/Sonidos/ElfoMuerte.wav");
        return MuerElf;
    }
}
