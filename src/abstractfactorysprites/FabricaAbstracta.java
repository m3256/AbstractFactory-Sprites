package abstractfactorysprites;

public interface FabricaAbstracta {

    public Salto crearSalto();

    public Izquierda crearIzquierda();

    public Derecha crearDerecha();

    public Ataque crearAtaque();
    
    public Muerte crearMuerte();
}
